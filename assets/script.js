const mymap = L.map('map').setView([45.7833, 3.0833], 12);

let darkMap = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWF0cmljaXMiLCJhIjoiY2tocnowcGY2MG52ZzJxcGJmZmd1Nm5tMCJ9.NIJR8RUpXw9lihmVkb7d5Q', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    minZoom: 5,
    maxZoom: 20,
    id: 'mapbox/dark-v10',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

let satelliteMap =
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWF0cmljaXMiLCJhIjoiY2tocnowcGY2MG52ZzJxcGJmZmd1Nm5tMCJ9.NIJR8RUpXw9lihmVkb7d5Q', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        minZoom: 5,
        maxZoom: 20,
        id: 'mapbox/satellite-streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'your.mapbox.access.token'
    });

let outdoors =
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWF0cmljaXMiLCJhIjoiY2tocnowcGY2MG52ZzJxcGJmZmd1Nm5tMCJ9.NIJR8RUpXw9lihmVkb7d5Q', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        minZoom: 5,
        maxZoom: 20,
        id: 'mapbox/outdoors-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'your.mapbox.access.token'
    });

let contact = '';
contact = L.marker([45.7772, 3.0870]);
contact.bindPopup("<div class='col text-center'><h4>AC Company</h4><p>25 rue de la fougère</p><p>63000 CLERMONT-FERRAND</p><p><strong>04.72.58.XX.XX</strong></p></div>");

mymap.addLayer(contact);

L.control.layers({
    'Vue black': darkMap,
    'Vue satellite': satelliteMap,
    'Vue outdoors': outdoors
}).addTo(mymap);