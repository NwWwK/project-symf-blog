<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\PostSearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostSearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => "Titre de l'article",
                'required' => false,
                'attr' => [
                    'placeholder' => 'Titre de l\'article'
                ]
            ])

            ->add('vip', ChoiceType::class, [
                'label' => 'Article exclusif',
                'required' => false,
                'choices' => [
                    'Oui' => true,
                    'Non' => false
                ]
            ])

            ->add('category', EntityType::class, [
                'label' => 'Catégorie',
                'required' => false,
                'class' => Category::class,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'mapped' => false,
                'expanded' => false,
                'multiple' => false
            ])

            ->add('publishStart', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date début (jj/mm/aaaa)',
                'required' => false,
                'format' => 'dd/MM/yyyy',
                'html5' => false,

            ])

            ->add('publishEnd', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date fin (jj/mm/aaaa)',
                'required' => false,
                'placeholder' => ['year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'],
                'format' => 'dd/MM/yyyy',
                'html5' => false,
            ])

            ->add('limit', ChoiceType::class, [
                'required' => false,
                'label' => 'Résultats par page :',
                'choices' => [
                    '5' => 5,
                    '15' => 15,
                    '30' => 30,
                    '50' => 50,
                    '75' => 75,
                    '100' => 100,
                    '150' => 150
                ],
            ])

            ->add('rechercher', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm btn-dark',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostSearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix(): string
    {
        return '';
    }
}
