<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Nom :',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Nom de famille'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre nom'
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Prénom :',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Prénom(s)'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre prénom'
                    ])
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email :',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Adresse email'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner votre adresse email'
                    ])
                ]
            ])
            ->add('subject', TextType::class, [
                'label' => 'Objet :',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Objet de votre message'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner l\'objet de votre message'
                    ])
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message :',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un message'
                    ])
                ]
            ])
            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-dark',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
