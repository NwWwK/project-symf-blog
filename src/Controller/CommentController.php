<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\CommentSearch;
use App\Form\CommentFormType;
use App\Form\CommentSearchFormType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use phpDocumentor\Reflection\Types\This;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CommentController extends AbstractController
{
    /**
     * Displays all comments with pagination
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/commentaires", name="app_admin_get_comments")
     * @param CommentRepository $commentRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return mixed
     */
    public function getComments(CommentRepository $commentRepository, PaginatorInterface $paginator, Request $request)
    {
        $search = new CommentSearch();

        $form = $this->createForm(CommentSearchFormType::class, $search);
        $form->handleRequest($request);

        $comments = $paginator->paginate(
            $commentRepository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            ($search->getLimit() ? $search->getLimit() : 10)
        );

        return $this->render('comment/index.html.twig', [
            'comments' => $comments,
            'form' => $form->createView(),
        ]);

    }

    /**
     * Update a comment
     * @Route("/commentaire/edition/{slug}/{id}", name="update_comment")
     * @Security("is_granted('ROLE_EDITOR') or user === comment.getUser()",
     *     message="Vous n'avez pas les droits pour modifier ce commentaire !")
     * @param Comment $comment
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param string $slug
     * @return Response
     */
    public function updateComment(
        Comment $comment,
        Request $request,
        EntityManagerInterface $em,
        string $slug
    ): Response {
        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Commentaire mis à jour.'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        return $this->render('comment/comment.html.twig', [
            'form' => $form->createView(),
            'comment' => $comment,
        ]);
    }

    /**
     * Delete comment
     * @Route("/commentaire/{slug}/{id}/{_csrf_token}", name="app_delete_comment")
     * @Security("is_granted('ROLE_EDITOR') or user === comment.getUser()",
     *     message="Vous n'avez pas les droits pour supprimer ce commentaire !")
     * @param Comment $comment
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param CsrfTokenManagerInterface $tokenManager
     * @param string $slug
     * @return mixed
     */
    public function deleteComment(
        Comment $comment,
        Request $request,
        EntityManagerInterface $em,
        CsrfTokenManagerInterface $tokenManager,
        string $slug
    ) {
        $token = new CsrfToken('delete_comment', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre commentaire a été supprimé.'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $this->addFlash(
            'danger',
            'Token invalide.'
        );

        return $this->redirectToRoute('get_post_public', ['slug' => $slug]);
    }
}
