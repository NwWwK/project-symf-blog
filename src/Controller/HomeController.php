<?php

namespace App\Controller;

use App\Entity\PostSearch;
use App\Form\ContactFormType;
use App\Form\PostSearchFormType;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Index page with formContact
     * @Route("/", name="home")
     * @param PostRepository $postRepository
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return Response
     */
    public function index(PostRepository $postRepository, Request $request, Swift_Mailer $mailer): Response
    {
        $posts = $postRepository->findBy([], ['id' => 'DESC'], 3);

        $form = $this->createForm(ContactFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataContact = $form->getData();

            $message = (new Swift_Message('Contact - AC Blog ' . $dataContact['subject']))
                ->setFrom($dataContact['email'])
                ->setTo('admin@gmail.com')
                ->setBody(
                    $this->renderView(
                        'emails/contact.html.twig', ['datacontact' => $dataContact]
                    ),
                    'text/html'
                );
            $mailer->send($message);

            $this->addFlash(
                'success',
                'Votre message a été envoyé !'
            );

            return $this->redirectToRoute('home');
        }

        return $this->render('home/index.html.twig', [
            'posts' => $posts,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/recherche", name="app_search")
     * SearchNavBar
     * @param Request $request
     * @param PostRepository $postRepository
     * @param PaginatorInterface $paginator
     * @return mixed
     */
    public function searchBar(Request $request, PostRepository $postRepository, PaginatorInterface $paginator)
    {
        $search = new PostSearch();

        $form = $this->createForm(PostSearchFormType::class, $search);
        $form->handleRequest($request);

        $posts = $paginator->paginate(
            $postRepository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            ($search->getLimit() ? $search->getLimit() : 10)
        );

        return $this->render('general/navbar.html.twig');
    }

    /**
     * Test css Page
     * @Route("/test", name="test")
     */
    public function test()
    {
        return $this->render('admin/test.html.twig');
    }

    /**
     * Display dashbooard
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/administration/dashboard", name="app_admin_dashboard")
     * @param PostRepository $postRepository
     * @return mixed
     */
    public function dashBoard(PostRepository $postRepository)
    {
        $posts = $postRepository->findBy([], ['publishAt' => 'ASC']);
        $totalPostsByCategory = $postRepository->countByCategory();

        return $this->render('admin/tdb.html.twig', [
            'posts' => $posts,
            'totalPostsByCategory' => $totalPostsByCategory,
        ]);
    }

    public function searchPost(PostRepository $postRepository, Request $request, PaginatorInterface $paginator) {

    }
}
