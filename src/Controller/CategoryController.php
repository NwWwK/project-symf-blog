<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class CategoryController extends AbstractController
{
    /**
     * Displays all categories with pagination
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categories", name="app_admin_categories")
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render('category/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * Create a category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/creer", name="app_admin_create_category")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function createCategory(Request $request, EntityManagerInterface $em)
    {
        $category = new Category();

        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'La catégorie a été créé'
            );

            return $this->redirectToRoute('app_admin_categories');
        }

        return $this->render('category/category.html.twig', [
            'form' => $form->createView(),
            'operation' => 'Créer'
        ]);
    }

    /**
     * Update a category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/modifier/{id}", name="app_admin_update_category")
     * @param Request $request
     * @param Category $category
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function updateCategory(Request $request, Category $category, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            $this->addFlash(
                'success',
                'L\'article a été modifié.'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        return $this->render('category/category.html.twig', [
            'operation' => 'Modifier',
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete Category
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour afficher cette page !")
     * @Route("/categorie/suppression/{id}/{_csrf_token}", name="app_admin_delete_category")
     * @param Category $category
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return mixed
     */
    public function deleteCategory(
        Category $category,
        EntityManagerInterface $em,
        Request $request,
        CsrfTokenManagerInterface $tokenManager
    ) {
        $token = new CsrfToken('delete_category', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($category);
            $em->flush();

            $this->addFlash(
                'success',
                'Catégorie supprimé'
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $this->addFlash(
            'danger',
            'Token invalide'
        );

        return $this->redirectToRoute('app_admin_categories');
    }

}
