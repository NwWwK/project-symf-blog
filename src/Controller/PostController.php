<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\PostSearch;
use App\Form\CommentFormType;
use App\Form\PostFormType;
use App\Form\PostSearchFormType;
use App\Form\SearchPostType;
use App\Manager\PostManager;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class PostController extends AbstractController
{
    private PostManager $postManager;

    public function __construct(PostManager $postManager)
    {
        $this->postManager = $postManager;
    }

    /**
     * Displays the list of paginated articles
     * @Route("/articles", name="get_posts")
     * @param PostRepository $postRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function getAllPosts(
        PostRepository $postRepository,
        Request $request,
        PaginatorInterface $paginator,
        EntityManagerInterface $em
    ): Response {

        $search = new PostSearch();

        $form = $this->createForm(PostSearchFormType::class, $search);
        $form->handleRequest($request);

        $category = $form->get('category')->getdata();

        if (isset($category)) {
            $search->setCategory($category->getID());
        }

        $posts = $paginator->paginate(

            $postRepository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            ($search->getLimit() ? $search->getLimit() : 10)
        );

        return $this->render('post/index.html.twig', [
            'posts' => $posts,
            'form' => $form->createView()
        ]);

    }

    /**
     * Get vip posts
     * @Route("/articles/VIP/", name="get_posts_vip")
     * @Security("is_granted('ROLE_USER')",
     *     message="Vous n'avez pas les droits pour consulter les articles exclusifs !")
     * @param PostRepository $postRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return mixed
     */
    public function getPostsVip(PostRepository $postRepository, Request $request, PaginatorInterface $paginator)
    {
        $posts = $paginator->paginate(
            $postRepository->findBy([
                'vip' => 1
            ],
                [
                    'id' => 'DESC',
                ]),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('post/index.html.twig', [
            'posts' => $posts
        ]);

    }

    /**
     * Create post
     * @route("/article/nouveau", name="create_post")
     * @Security("is_granted('ROLE_EDITOR')",
     *     message="Vous n'avez pas les droits pour rédiger un article !")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function createPost(Request $request, EntityManagerInterface $em)
    {
        $post = new Post();

        $formPost = $this->createForm(PostFormType::class, $post);
        $formPost->handleRequest($request);

        if ($formPost->isSubmitted() && $formPost->isValid()) {
            if ($this->isGranted('ROLE_EDITOR')) {

                $post
                    ->setPublishAt(new DateTime('now'))
                    ->setCategories($formPost->get('category')->getData())
                    ->setUser($this->getUser());

                $em->persist($post);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Votre article a été publié'
                );

                return $this->redirectToRoute('get_posts');
            }
        }

        return $this->render('post/create_post.html.twig', [
            'formPost' => $formPost->createView()
        ]);
    }

    /**
     * Display post
     * @Route("/articles/{slug}", name="get_post_public")
     * @param Post $post
     * @param PostRepository $postRepository
     * @param CommentRepository $commentRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function getPost(
        Post $post,
        PostRepository $postRepository,
        CommentRepository $commentRepository,
        Request $request,
        PaginatorInterface $paginator,
        EntityManagerInterface $em
    ) {
        $var = [
            'slug' => $post->getSlug()
        ];

        if ($post->getVip() != 0 && !$this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_login');
        }

        $post = $postRepository->findOneBy($var);
        $posts = $postRepository->findBy([], ['id' => 'DESC'], 3);


        $comments = $paginator->paginate(
            $commentRepository->findBy(['post' => $post], ['id' => 'DESC']),
            $request->query->getInt('page', 1),
            3
        );

        // Add comment
        $comment = new Comment();
        $formComment = $this->createForm(CommentFormType::class, $comment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $comment
                ->setPublishAt(new DateTime('now'))
                ->setUser($this->getUser())
                ->setPost($post);

            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre commentaire a eté posté !'
            );

            return $this->redirectToRoute('get_post_public', ['slug' => $post->getSlug()]);
        }

        $var = [
            'post' => $post,
            'posts' => $posts,
            'comments' => $comments,
        ];

        if ($this->isGranted('ROLE_USER')) {
            $var['formComment'] = $formComment->createView();
        }

        return $this->render('post/post.html.twig', $var);
    }

    /**
     * Update post
     * @Route("/article/edition/{slug}", name="update_post")
     * @Security("is_granted('ROLE_EDITOR') or user === post.getUser()",
     *     message="Vous n'avez pas les droits pour modifier cet article !")
     * @param Post $post
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function updatePost(Post $post, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $categories = $form->get('category')->getData();
            $post->setCategories($categories);

            $em->persist($post);
            $em->flush();

            $this->addFlash(
                'success',
                'Article mis à jour.'
            );

            return $this->redirectToRoute('get_post_public', ['slug' => $post->getSlug()]);

        } else {
            $currentCategories = $post->getCategories();
            $formCategories = new ArrayCollection();

            foreach ($currentCategories as $category) {
                $formCategories->add($category);
            }
            $form->get('category')->setData($formCategories);
        }

        return $this->render('post/create_post.html.twig', [
            'formPost' => $form->createView()
        ]);
    }

    /**
     * Delete Post
     * @Security("is_granted('ROLE_EDITOR') and user === post.getUser() or is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour supprimer cet article !")
     * @Route("/article/suppression/{slug}/{_csrf_token}", name="delete_post")
     * @param Post $post
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return mixed
     */
    public function deletePost(
        Post $post,
        EntityManagerInterface $em,
        Request $request,
        CsrfTokenManagerInterface $tokenManager
    ) {
        $token = new CsrfToken('delete_post', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($post);
            $em->flush();

            $this->addFlash(
                'success',
                "L'article {$post->getTitle()} a été supprimé"
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $this->addFlash(
            'danger',
            'Token invalide !'
        );

        return $this->redirectToRoute('get_posts');
    }

    /**
     * List of posts
     * @Route("/administration/articles", name="app_admin_posts")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param PostRepository $postRepository
     * @return Response
     */
    public function getListOfPosts(
        Request $request,
        PaginatorInterface $paginator,
        PostRepository $postRepository
    ): Response {

        $search = new PostSearch();

        $form = $this->createForm(PostSearchFormType::class, $search);
        $form->handleRequest($request);

        $category = $form->get('category')->getdata();

        if (isset($category)) {
            $search->setCategory($category->getID());
        }

        $posts = $paginator->paginate(
            $postRepository->findAllVisibleQuery($search),
            $request->query->getInt('page', 1),
            ($search->getLimit() ? $search->getLimit() : 10)
        );

        return $this->render('post/admin_posts.html.twig', [
            'posts' => $posts,
            'form' => $form->createView()
        ]);
    }

    /**
     * Get posts by category
     * @Route("/articles/categorie/{category}", name="app_posts_by_category")
     * @param PostRepository $postRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param string $category
     * @return mixed
     */
    public function getPostsByCategory(
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        Request $request,
        string $category
    ): Response {
        $posts = $paginator->paginate(
            $postRepository->findByCategory($category),
            $request->query->getInt('page', 1)
        );

        return $this->render('post/index.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route("/search", name="post_search")
     * @param PostRepository $postRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return mixed
     */
    public function searchPost(
        PostRepository $postRepository,
        PaginatorInterface $paginator,
        Request $request

    ): Response {
        $search = new PostSearch();

        $form = $this->createForm(SearchPostType::class, $search,
            ['action' => $this->generateUrl('post_search')]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $category = $form->get('category')->getdata();

            if (isset($category)) {
                $search->setCategory($category->getId());
            }

            $posts = $paginator->paginate(
                $postRepository->findAllVisibleQuery($search),
                $request->query->getInt('page', 1)
            );

            return $this->render('post/index.html.twig', [
                'posts' => $posts,
                'search' => $search
            ]);
        }

        return $this->render('general/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
