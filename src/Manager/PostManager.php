<?php

namespace App\Manager;

use App\Entity\PostSearch;
use App\Form\PostSearchFormType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\Request;

class PostManager
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * @var PostRepository
     */
    private PostRepository $postRepository;

    /**
     * PostManager contructor
     * @param EntityManagerInterface $em
     * @param PostRepository $postRepository
     */
    public function __construct(EntityManagerInterface $em, PostRepository $postRepository)
    {
        $this->em = $em;
        $this->postRepository = $postRepository;
    }

}
