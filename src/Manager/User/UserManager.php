<?php

namespace App\Manager\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class UserManager
{
    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * @var UserRepository $userRepository
     */
    private UserRepository $userRepository;


    /**
     * UserManager constructor
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     */
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * Block user
     * @Security("is_granted('ROLE_ADMIN')",
     *     message="Vous n'avez pas les droits pour cette fonctionnalité !")
     * @param User $user
     */
    public function blockUser(User $user)
    {
        // Check if user is enabled
        $enabled = $user->getIsEnabled();

        //Change the boolean
        ($enabled ? $user->setIsEnabled('0') : $user->setIsEnabled('1'));

        //Persist and flush
        $this->em->persist($user);
        $this->em->flush();
    }
}
