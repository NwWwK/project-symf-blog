<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * Class PostSearch
 * @package App\Entity
 */
class PostSearch
{
    /**
     * @var string|null
     */
    private ?string $title = null;

    /**
     * @var bool|null
     */
    private ?bool $vip = null;

    /**
     * @var null
     */
    private $category = null;

    /**
     * @var null
     */
    private $publishStart = null;

    /**
     * @var null
     */
    private $publishEnd = null;

    /**
     * @var int|null
     */
    private ?int $limit = null;


    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getVip()
    {
        return $this->vip;
    }

    /**
     * @param null $vip
     */
    public function setVip($vip): void
    {
        $this->vip = $vip;
    }

    /**
     * @return int|null
     */
    public function getCategory(): ?int
    {
        return $this->category;
    }

    /**
     * @param null $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return null
     */
    public function getPublishStart()
    {
        return $this->publishStart;
    }

    /**
     * @param null $publishStart
     */
    public function setPublishStart($publishStart): void
    {
        $this->publishStart = $publishStart;
    }

    /**
     * @return null
     */
    public function getPublishEnd()
    {
        return $this->publishEnd;
    }

    /**
     * @param null $publishEnd
     */
    public function setPublishEnd($publishEnd): void
    {
        $this->publishEnd = $publishEnd;
    }

    /**
     * @return int
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }
}