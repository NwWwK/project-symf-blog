<?php

namespace App\Entity;


/**
 * Class CommentSearch
 * @package App\Entity
 */
class CommentSearch {

    /**
     * @var string|null
     */
    private ?string $title = null;
    /**
     * @var null
     */
    private $publishStart = null;

    /**
     * @var null
     */
    private $publishEnd = null;

    /**
     * @var int|null
     */
    private ?int $limit = null;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getPublishStart()
    {
        return $this->publishStart;
    }

    /**
     * @param null $publishStart
     */
    public function setPublishStart($publishStart): void
    {
        $this->publishStart = $publishStart;
    }

    /**
     * @return null
     */
    public function getPublishEnd()
    {
        return $this->publishEnd;
    }

    /**
     * @param null $publishEnd
     */
    public function setPublishEnd($publishEnd): void
    {
        $this->publishEnd = $publishEnd;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }


}


