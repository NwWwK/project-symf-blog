<?php

namespace App\Repository;

use App\Entity\Comment;
use App\Entity\CommentSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function findAllVisibleQuery(CommentSearch $commentSearch): Query
    {
        $query = $this->findVisibleQuery();

        if ($commentSearch->getTitle()) {
            $query = $query
                ->andWhere('c.title LIKE :title')
                ->setParameter('title', '%' . $commentSearch->getTitle() . '%');
        }

        if ($commentSearch->getPublishStart()) {
            $query = $query
                ->andWhere('c.publishAt >= :publishStart')
                ->setParameter('publishStart', $commentSearch->getPublishStart());
        }

        if ($commentSearch->getPublishEnd()) {
            $query = $query
                ->andWhere('c.publishAt <= :publishEnd')
                ->setParameter('publishEnd', $commentSearch->getPublishEnd());
        }

        if ($commentSearch->getLimit()) {
            $query = $query
                ->setMaxResults($commentSearch->getLimit());
        }

        return $query->getQuery();

    }


    public function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->orderBy('c.publishAt', 'DESC');
    }

    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
