<?php

namespace App\Repository;

use App\Entity\Post;
use App\Entity\PostSearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function findAllVisibleQuery(PostSearch $postSearch): Query
    {
        $query = $this->findVisibleQuery();

        if ($postSearch->getTitle()) {
            $query = $query
                ->andWhere('p.title LIKE :title')
                ->setParameter('title', '%' . $postSearch->getTitle() . '%');
        }

        if ($postSearch->getCategory()) {
            $query = $query
                ->join('p.categories', 'c')
                ->andWhere('c.id = :categoryId')
                ->setParameter('categoryId', $postSearch->getCategory());
        }

        if ($postSearch->getVip() === true) {
            $query = $query
                ->andWhere('p.vip = :vip')
                ->setParameter('vip', true);

        } elseif ($postSearch->getVip() === false) {
            $query = $query
                ->andWhere('p.vip = :vip')
                ->setParameter('vip', false);
        }

        if ($postSearch->getPublishStart()) {
            $query = $query
                ->andWhere('p.publishAt >= :publishStart')
                ->setParameter('publishStart', $postSearch->getPublishStart());
        }


        if ($postSearch->getPublishEnd()) {
            $query = $query
                ->andWhere('p.publishAt <= :publishEnd')
                ->setParameter('publishEnd', $postSearch->getPublishEnd());
        }

        if ($postSearch->getLimit()) {
            $query = $query
                ->setMaxResults($postSearch->getLimit());
        }

        return $query->getQuery();
    }

    public function findVisibleQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->orderBy('p.id', 'DESC');
    }

    public function findByCategory($category)
    {
        $query = $this->findVisibleQuery();

        if (isset($category)) {
            $query = $query
                ->join('p.categories', 'c')
                ->andWhere('c.name = :categoryName')
                ->setParameter('categoryName', $category);
        }

        return $query->getQuery();
    }

    public function countByCategory()
    {
        $query = $this->findVisibleQuery();

        $query = $query
            ->select("COUNT(p.id) AS count")
            ->join('p.categories', 'c')
            ->addSelect('c.name')
            ->groupBy('c.name');

        return $query->getQuery()->getArrayResult();
    }

    public function findByUser($slug)
    {
        $query = $this->findVisibleQuery();

        if (isset($category)) {
            $query = $query
                ->join('p.user', 'u')
                ->andWhere('u.slug = :slug')
                ->setParameter('slug', $slug)
                ->orderBy('p.id => DESC');
        }

        return $query->getQuery();
    }

    // /**
    //  * @return Post[] Returns an array of Post objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
