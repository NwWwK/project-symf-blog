<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('FR-fr');

        // Fixtures User
        $user = new User();
        $password = $this->encoder->encodePassword($user, 'password');
        $user
            ->setFirstName('Admin')
            ->setLastName('Admin')
            ->setEmail('admin@gmail.com')
            ->setPassword($password)
            ->setIsVerified(1)
            ->setIsEnabled(1)
            ->setRoles(["ROLE_ADMIN"]);

        $manager->persist($user);
        $users[] = $user;

        for ($i = 1; $i <= 25; $i++) {
            $user = new User();

            $password = $this->encoder->encodePassword($user, 'password');

            $user
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setEmail($faker->email)
                ->setPassword($password)
                ->setIsVerified(mt_rand(0, 1))
                ->setIsEnabled(mt_rand(0, 1))
                ->setRoles(["ROLE_USER"]);

            $manager->persist($user);
            $users[] = $user;
        }

        $manager->flush($users);

        //Fixtures category
        for ($i = 1; $i <= 5; $i++) {

            $category = new Category();

            $category->setName($faker->word());

            $manager->persist($category);
            $categories[] = $category;
        }
        $manager->flush($categories);

        //Fixtures post
        for ($i = 1; $i <= 90; $i++) {

            $post = new Post();

            $user = $users[mt_rand(0, count($users) - 1)];
            $category = $categories[mt_rand(0, count($categories) - 1)];

            $post
                ->setTitle($faker->sentence(3))
                ->setContent($faker->paragraph(5))
                ->setVip(mt_rand(0, 1))
                ->setPicture("https://picsum.photos/1200/400")
                ->setPublishAt(new DateTime($faker->date('Y-m-d')))
                ->setUser($user);

            $nb = mt_rand(1, 3);

            for ($j = 1; $j <= $nb; $j++) {
                $category = $categories[mt_rand(0, count($categories) - 1)];
                $post->addCategory($category);
            }

            $manager->persist($post);
            $posts[] = $post;
        }
        $manager->flush($posts);

        //Fixtures comments
        for ($i = 1; $i <= 200; $i++) {
            $comment = new Comment();

            $user = $users[mt_rand(0, count($users) - 1)];
            $post = $posts[mt_rand(0, count($posts) - 1)];

            $comment
                ->setTitle($faker->sentence(4))
                ->setContent($faker->paragraph(2))
                ->setUser($user)
                ->setPost($post)
                ->setPublishAt(new DateTime($faker->date('Y-m-d')));

            $manager->persist($comment);
            $comments[] = $comment;
        }
        $manager->flush($comments);

    }
}
