###AC Blog

##Clone the project
```git clone https://gitlab.com/NwWwK/ac-blog.git```

##Outbuildings facilities
```composer install```

```npm install```

```yarn install && yarn dev```

##Project setup

Configure the .env

```php bin/console doctrine:database:create```

```php bin/console doctrine:migration:migrate```

```php bin/console doctrine:fixtures:load```

##Account and password
``` Admin => 'admin@gmail.com' => 'password' ```

``` Other accounts => email => 'password' ```